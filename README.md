# Setup

1.  Download git repository: manually ("clone" button above) or through command as below:

- `git clone https://hanwenchong@bitbucket.org/hanwenchong/grocery-app-frontend.git`

2.  Installation: `npm install`
3.  Run App: `npm run dev`
