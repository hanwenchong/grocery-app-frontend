import authConstant from 'constants/AuthConstant';

export const initialState = {
  error: '',
  loading: false,
  isAuthenticated: false,
  user: null,
  permission: null,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authConstant.SENDING_REQUEST:
    case authConstant.SET_AUTH:
    case authConstant.REQUEST_ERROR:
    case authConstant.CLEAR_ERROR: {
      return { ...state, ...action.payload };
    }
    default:
      return state;
  }
};
