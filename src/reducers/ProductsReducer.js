import constant from 'constants/ProductsConstant';

export const initialState = {
  error: '',
  loading: false,
  products: null,
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case constant.SENDING_REQUEST:
    case constant.SET_PRODUCTS:
    case constant.REQUEST_ERROR:
    case constant.CLEAR_ERROR: {
      return { ...state, ...action.payload };
    }
    default:
      return state;
  }
};
