import React from 'react';
import PropTypes from 'prop-types';
// router
import { Route, Switch, useLocation } from 'react-router-dom';
// UI Framework
import { Spin } from 'antd';
// apis
import { getCurrentUser } from 'api/AuthAPI';
// context
import useAuth from 'contexts/AuthContext';
// actions
import authAction from 'actions/AuthAction';
// layouts
import AuthLayout from './layouts/AuthLayout';
import NotAuthLayout from './layouts/NotAuthLayout';
import RegisterPath from './layouts/RegisterPath';
import PublicLayout from './layouts/PublicLayout';
// maps
import permission from './permission';

export default function App() {
  const {
    state: { user, isAuthenticated, loading },
    dispatch,
  } = useAuth();
  const location = useLocation();

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  React.useEffect(() => {
    let ignore = false;
    const fetchUser = async () => {
      dispatch(authAction.sendingRequest({ loading: true }));
      try {
        const payload = await getCurrentUser();
        const auth = {
          user: payload.user,
          permission: permission[payload.user.role],
          isAuthenticated: true,
        };

        if (!ignore) {
          dispatch(authAction.setAuth({ ...auth }));
          dispatch(authAction.sendingRequest({ loading: false }));
        }
      } catch (response) {
        if (!ignore) {
          dispatch(authAction.sendingRequest({ loading: false }));
          dispatch(authAction.requestError({ error: response }));
        }
      }
    };

    if (!user && isAuthenticated) {
      fetchUser();
    }

    return () => {
      ignore = true;
    };
  }, [dispatch, isAuthenticated, user]);

  if (!user && isAuthenticated) {
    return null;
  }
  const SpinWrapper = props => {
    const { tip } = props;
    return (
      <div className="spin-wrapper">
        <Spin tip={tip} />
      </div>
    );
  };

  SpinWrapper.propTypes = {
    tip: PropTypes.string.isRequired,
  };

  return (
    <Switch>
      {loading ? (
        <SpinWrapper tip="load user..." />
      ) : (
        <React.Suspense fallback={<SpinWrapper tip="load content..." />}>
          <Route path="/" component={props => <RegisterPath {...props} user={user} />} />
          <Route path="/admin" component={AuthLayout} />
          <Route path="/auth" component={NotAuthLayout} />
          <Route path="/public" component={PublicLayout} />
          <Route
            path="/403"
            component={React.lazy(() => import('./views/ExceptionPage/ForbiddenPage'))}
          />
          <Route
            path="/500"
            component={React.lazy(() => import('./views/ExceptionPage/ServerErrorPage'))}
          />
        </React.Suspense>
      )}
    </Switch>
  );
}
