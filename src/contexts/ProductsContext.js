import React from 'react';
import { productsReducer, initialState } from 'reducers/ProductsReducer';

const ProductsContext = React.createContext({
  state: initialState,
  dispatch: () => initialState,
});

/**
 *
 */
export function ProductsProvider(props) {
  const [state, dispatch] = React.useReducer(productsReducer, initialState);
  return <ProductsContext.Provider value={{ state, dispatch }} {...props} />;
}

/**
 *
 */
export default function useProducts() {
  const context = React.useContext(ProductsContext);
  if (!context) {
    throw new Error(`useProducts must be used within an ProductsProvider`);
  }
  return context;
}
