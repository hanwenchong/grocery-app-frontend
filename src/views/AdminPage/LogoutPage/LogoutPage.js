import React from 'react';
import { useHistory } from 'react-router-dom';
// API
import useAuth from 'contexts/AuthContext';
import authAction from 'actions/AuthAction';
import { logout } from 'api/AuthAPI';

/**
 *
 */
export default function LogoutPage() {
  const { dispatch } = useAuth();
  const history = useHistory();

  React.useEffect(() => {
    let ignore = false;
    if (!ignore) {
      dispatch(authAction.sendingRequest({ loading: true }));
      dispatch(authAction.setAuth({ user: null, isAuthenticated: false }));
    }
    logout();
    if (!ignore) {
      dispatch(authAction.sendingRequest({ loading: false }));
    }
    history.push('/auth/login');

    return () => {
      ignore = true;
    };
  }, [dispatch, history]);

  return null;
}
