import React from 'react';
import { Link } from 'react-router-dom';
// UI Framework
import { Breadcrumb, PageHeader } from 'antd';

/**
 *
 */
export default function DashboardPage() {
  return (
    <div style={{ margin: '-24px -24px 0' }}>
      <div className="page--header">
        <div className="page--wrapper">
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/admin/dashboard">Home</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
          </Breadcrumb>
          <PageHeader style={{ padding: 0 }} title="Dashboard" />
        </div>
      </div>
      <div className="content">Content</div>
    </div>
  );
}
