import React from 'react';
import { Link } from 'react-router-dom';
import shortid from 'shortid';
import { useMediaQuery } from 'react-responsive';
// UI Framework
import { PageHeader, Table, Breadcrumb, Button, Popconfirm, Icon, Divider } from 'antd';
// Redux components
import useAuth from 'contexts/AuthContext';
import useProducts, { ProductsProvider } from 'contexts/ProductsContext';
import productsAction from 'actions/ProductsAction';
import { getProducts, deleteProduct } from 'api/ProductsAPI';
// utils
import { media } from 'utils/media';
// Components
import ProductModal from './ProductModal';
import SearchFilterForm from './SearchFilterForm';

const ProductListPage = props => {
  const productState = useProducts().state;
  const productDispatch = useProducts().dispatch;
  const authState = useAuth().state;
  const accessible = authState.permission.productsPage;
  const isLargeScreen = useMediaQuery({ maxWidth: media.lg });

  const initialPaginationState = {
    total: 0,
    showSizeChanger: true,
    showQuickJumper: true,
    showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
    current: 1,
    pageSize: 10,
  };
  const [paginationState, setPaginationState] = React.useState(initialPaginationState);

  async function fetchProducts(params = { pagination: initialPaginationState }) {
    productDispatch(productsAction.sendingRequest({ loading: true }));
    try {
      const data = await getProducts(params);
      productDispatch(productsAction.setProducts({ products: data.products }));
      setPaginationState({
        ...params.pagination,
        total: data.total,
      });
      productDispatch(productsAction.sendingRequest({ loading: false }));
    } catch (err) {
      productDispatch(productsAction.sendingRequest({ loading: false }));
      productDispatch(productsAction.requestError({ error: err.data }));
    }
  }

  const initialCallbackState = {
    cb: fetchProducts,
  };
  const [callbackState] = React.useState(initialCallbackState);

  async function deleteProductById(productId) {
    productDispatch(productsAction.sendingRequest({ loading: true }));
    try {
      await deleteProduct(productId);
      productDispatch(productsAction.sendingRequest({ loading: false }));
      await callbackState.cb();
    } catch (err) {
      productDispatch(productsAction.sendingRequest({ loading: false }));
      productDispatch(productsAction.requestError({ error: err.data }));
    }
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      defaultSortOrder: 'ascend',
      key: 'name',
      width: isLargeScreen ? 200 : null,
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      sorter: true,
      key: 'brand',
      width: isLargeScreen ? 200 : null,
    },
    {
      title: 'Barcode',
      dataIndex: 'barcode',
      sorter: true,
      key: 'barcode',
      width: isLargeScreen ? 200 : null,
    },
    {
      title: 'Action',
      key: 'action',
      align: 'center',
      className: !accessible.edit ? 'hide' : null,
      width: 200,
      fixed: isLargeScreen && 'right',
      render: (text, record) => (
        <span>
          <ProductModal title="Edit" data={record} callback={callbackState.cb} />
          <Divider type="vertical" />
          <Popconfirm
            title="Are you sure to delete?"
            icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
            // eslint-disable-next-line no-underscore-dangle
            onConfirm={async () => deleteProductById(record._id)}
          >
            <Button type="link">Delete</Button>
          </Popconfirm>
        </span>
      ),
    },
  ];
  async function handleTableOnChange(pagination, filters, sorter) {
    await callbackState.cb({
      pagination,
      sorter,
      filters,
    });
  }

  React.useEffect(() => {
    let ignore = false;

    if (!ignore) {
      callbackState.cb();
    }
    return () => {
      ignore = true;
    };
  }, [callbackState]);

  return (
    <div style={{ margin: '-24px -24px 0' }}>
      <div className="page--header">
        <div className="page--wrapper">
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/admin/dashboard">Home</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Product</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
          <PageHeader style={{ padding: 0 }} title="Product List" />
        </div>
      </div>
      <div className="content">
        <SearchFilterForm callback={callbackState.cb} />
        {accessible.create && <ProductModal title="Add Product" callback={callbackState.cb} />}
        <Table
          size="small"
          loading={productState.loading}
          rowKey={shortid.generate}
          columns={columns}
          dataSource={productState.products}
          pagination={paginationState}
          onChange={handleTableOnChange}
          scroll={{ x: 600 }}
        />
      </div>
    </div>
  );
};

export default () => (
  <ProductsProvider>
    <ProductListPage />
  </ProductsProvider>
);
