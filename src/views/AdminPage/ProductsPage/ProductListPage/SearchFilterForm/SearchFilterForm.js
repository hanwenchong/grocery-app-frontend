import React from 'react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';
// UI Framework
import { Form, Input, Button, Row, Col, Icon } from 'antd';
// utils
import { media } from 'utils/media';

const SearchFilterForm = props => {
  const { form, callback } = props;
  const isMobile = useMediaQuery({ maxWidth: media.sm });
  const [loadingState, setLoadingState] = React.useState(false);
  const [expandState, setExpandState] = React.useState(false);

  const handleOnSubmit = async e => {
    e.preventDefault();

    const formState = await new Promise(resolve => {
      form.validateFields(async (err, values) => {
        console.log('Received values of form: ', values);
        if (!err) {
          resolve(values);
        }
      });
    });

    try {
      setLoadingState(true);
      await callback({ search: formState });
      setLoadingState(false);
    } catch (err) {
      setLoadingState(false);
    }
  };

  const handleReset = async () => {
    form.resetFields();
    await callback();
  };
  const toggle = () => {
    setExpandState(!expandState);
  };

  const { getFieldDecorator } = form;
  const searchFilterTools = (
    <>
      <Button type="primary" htmlType="submit" loading={loadingState}>
        Search
      </Button>
      <Button style={{ marginLeft: 8 }} onClick={handleReset}>
        Clear
      </Button>
      <Button type="link" style={{ marginLeft: 8, fontSize: 12 }} onClick={toggle}>
        {expandState ? `Collapse ` : `Expand `}
        <Icon type={expandState ? 'up' : 'down'} />
      </Button>
    </>
  );
  return (
    <Form onSubmit={handleOnSubmit} className={!isMobile ? 'ant-advanced-search-form' : null}>
      <Row gutter={24}>
        <Col xs={24} sm={12} md={8} lg={8}>
          <Form.Item label="Name">
            {getFieldDecorator('name')(<Input allowClear placeholder="Product Name" />)}
          </Form.Item>
        </Col>
        <Col xs={24} sm={12} md={8} lg={8}>
          <Form.Item label="Brand">
            {getFieldDecorator('brand')(<Input allowClear placeholder="Product Brand" />)}
          </Form.Item>
        </Col>
        <Col xs={24} sm={12} md={8} lg={8} style={{ display: expandState ? 'block' : 'none' }}>
          <Form.Item label="Barcode">
            {getFieldDecorator('barcode')(<Input allowClear placeholder="Barcode" />)}
          </Form.Item>
        </Col>
        <Col xs={24} sm={12} md={8} lg={8} style={{ display: !expandState ? 'block' : 'none' }}>
          <Form.Item>{!expandState && searchFilterTools}</Form.Item>
        </Col>
      </Row>
      <div style={{ overflow: 'hidden' }}>
        <div style={expandState ? { float: 'right', marginBottom: 24 } : null}>
          {expandState && searchFilterTools}
        </div>
      </div>
    </Form>
  );
};

SearchFilterForm.propTypes = {
  form: PropTypes.objectOf(PropTypes.func).isRequired,
  callback: PropTypes.func.isRequired,
};

export default Form.create()(SearchFilterForm);
