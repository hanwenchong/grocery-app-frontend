import React from 'react';
import PropTypes from 'prop-types';
// UI Framework
import { Modal, Form, Input, Button } from 'antd';
// Redux components
import useProducts from 'contexts/ProductsContext';
import productsAction from 'actions/ProductsAction';
import { createProduct, editProduct } from 'api/ProductsAPI';
// style
import { Wrapper } from './style';

const ProductModal = props => {
  const productState = useProducts().state;
  const productDispatch = useProducts().dispatch;

  const [modalState, setModalState] = React.useState(false);
  const { form, title, data, callback } = props;

  const showModal = async () => {
    setModalState(true);
  };
  const handleCancel = () => {
    setModalState(false);
  };

  const handleOnSubmit = async e => {
    e.preventDefault();

    const formState = await new Promise(resolve => {
      form.validateFields(async (err, values) => {
        console.log('Received values of form: ', values);
        if (!err) {
          resolve(values);
        }
      });
    });

    try {
      if (data) {
        await editProduct({
          ...formState,
          // eslint-disable-next-line no-underscore-dangle
          _id: data._id,
        });
      } else {
        await createProduct(formState);
      }
      productDispatch(productsAction.sendingRequest({ loading: false }));
      await callback();
      setModalState(false);
    } catch (err) {
      productDispatch(productsAction.sendingRequest({ loading: false }));
      productDispatch(productsAction.requestError({ error: err.data }));
    }
  };

  const { getFieldDecorator } = form;
  return (
    <>
      <Button
        type={data ? 'link' : 'primary'}
        onClick={showModal}
        style={data ? null : { marginBottom: '8px' }}
      >
        {title}
      </Button>
      <Modal
        visible={modalState}
        title={title}
        onOk={handleOnSubmit}
        onCancel={handleCancel}
        style={{ top: 20 }}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Return
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={productState.loading}
            onClick={handleOnSubmit}
          >
            Submit
          </Button>,
        ]}
      >
        <Wrapper>
          <Form onSubmit={handleOnSubmit}>
            <Form.Item label="Name">
              {getFieldDecorator('name', {
                initialValue: data ? data.name : undefined,
                rules: [
                  { required: true, message: 'Please input product name.' },
                  { type: 'string', message: 'Please input string data type.' },
                ],
              })(<Input allowClear placeholder="Product Name" />)}
            </Form.Item>
            <Form.Item label="Brand">
              {getFieldDecorator('brand', {
                initialValue: data ? data.brand : undefined,
                rules: [
                  { required: true, message: 'Please input product brand.' },
                  { type: 'string', message: 'Please input string data type.' },
                ],
              })(<Input allowClear placeholder="Product Brand" />)}
            </Form.Item>
            <Form.Item label="Barcode">
              {getFieldDecorator('barcode', {
                initialValue: data ? data.barcode : undefined,
              })(<Input allowClear placeholder="Barcode" type="number" />)}
            </Form.Item>
          </Form>
        </Wrapper>
      </Modal>
    </>
  );
};

ProductModal.propTypes = {
  form: PropTypes.objectOf(PropTypes.func).isRequired,
  title: PropTypes.string.isRequired,
  data: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    brand: PropTypes.string,
    barcode: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  callback: PropTypes.func.isRequired,
};
ProductModal.defaultProps = {
  data: null,
};

export default Form.create()(ProductModal);
