import React from 'react';
import { Result, Button } from 'antd';
import { Link } from 'react-router-dom';

const BackHomeButton = () => (
  <Button type="primary">
    <Link to="/">Back Home</Link>
  </Button>
);

export default () => (
  <Result
    status="500"
    title="500"
    subTitle="Sorry, the server is wrong."
    extra={<BackHomeButton />}
  />
);
