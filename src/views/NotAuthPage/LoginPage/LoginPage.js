import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
// UI Framework
import { Alert, Form, Icon, Input, Button, Spin } from 'antd';
// Redux components
import useAuth from 'contexts/AuthContext';
import authAction from 'actions/AuthAction';
import { login } from 'api/AuthAPI';
// permission
import permission from 'permission';
// style
import { Wrapper } from './style';

/**
 *
 */
const LoginPage = props => {
  const {
    state: { loading, error },
    dispatch,
  } = useAuth();
  const { form } = props;

  const handleOnSubmit = async e => {
    e.preventDefault();

    const formState = await new Promise(resolve => {
      form.validateFields(async (err, values) => {
        console.log('Received values of form: ', values);
        if (!err) {
          resolve(values);
        }
      });
    });
    if (formState) {
      try {
        dispatch(authAction.sendingRequest({ loading: true }));
        const authUser = await login(formState);
        const auth = {
          user: authUser,
          permission: permission[authUser.role],
          isAuthenticated: true,
        };
        dispatch(authAction.setAuth({ ...auth }));
        dispatch(authAction.sendingRequest({ loading: false }));
        dispatch(authAction.clearError());
      } catch (response) {
        const { data } = response;
        dispatch(authAction.sendingRequest({ loading: false }));
        dispatch(authAction.requestError({ error: data.message }));
      }
    }
  };

  const { getFieldDecorator } = form;
  return (
    <Wrapper>
      {error && <Alert message={error} type="error" banner closable />}
      <Spin spinning={loading}>
        <Form onSubmit={handleOnSubmit}>
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [
                { required: true, message: 'Please input your username.' },
                { type: 'string', message: 'Please input string data type.' },
              ],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                allowClear
                placeholder="Username"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Please input your password.' },
                { type: 'string', message: 'Please input string data type.' },
              ],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                allowClear
                placeholder="password"
              />
            )}
          </Form.Item>
          <Form.Item>
            <Link to="/public/forgot-password">Forgot password</Link>
            <Button type="primary" htmlType="submit" block>
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </Wrapper>
  );
};

LoginPage.propTypes = {
  form: PropTypes.objectOf(PropTypes.func).isRequired,
};

export default Form.create()(LoginPage);
