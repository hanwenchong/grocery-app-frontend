import imageCompression from 'browser-image-compression';

export const compress = async files => {
  const imageCompressionOptions = {
    maxSizeMB: 2,
    maxWidthOrHeight: 1024,
    useWebWorker: true,
  };
  const compressedFileList = [];
  const fileList = Array.isArray(files) ? files : [files];
  try {
    await Promise.all(
      fileList.map(async file => {
        const fileObj = file.originFileObj;
        if (fileObj instanceof File) {
          const compressedFile = await imageCompression(fileObj, imageCompressionOptions);
          compressedFileList.push(compressedFile);
        }
      })
    );
    return compressedFileList;
  } catch (error) {
    console.log(error);
    return error;
  }
};

export const getBase64 = async file => {
  if (file instanceof Blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = err => reject(err);
    });
  }
  return null;
};
