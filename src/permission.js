const permission = {
  SUPER_ADMIN: {
    productsPage: { create: true, read: true, edit: true, delete: true },
  },
  USER: {
    productsPage: { create: false, read: true, edit: false, delete: false },
  },
};

export default permission;
