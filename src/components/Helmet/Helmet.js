/**
 * Reusable Helmet
 *
 * Customize: title, favicon, manifest, icons, appleTouchIcons
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import shortid from 'shortid';

import { REACT_APP_TITLE } from 'config';

const ReHelmet = props => {
  const { title, icons, appleTouchIcons, favicon, manifest, children } = props;
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{`${title} - ${REACT_APP_TITLE || 'Grocery'}`}</title>
      <link rel="icon" href={favicon} />
      {icons.map(icon => (
        <link
          rel="icon"
          key={shortid.generate()}
          sizes={`${icon.size}x${icon.size}`}
          href={icon.href}
        />
      ))}
      {appleTouchIcons.map(icon => (
        <link
          rel="apple-touch-icon"
          key={shortid.generate()}
          sizes={`${icon.size}x${icon.size}`}
          href={icon.href}
        />
      ))}
      <link rel="manifest" href={manifest} />
      {children}
    </Helmet>
  );
};

ReHelmet.defaultProps = {
  title: 'Admin Platform',
  icons: [
    { size: 32, href: '/favicon_io/favicon-32x32.png' },
    { size: 16, href: '/favicon_io/favicon-16x16.png' },
  ],
  appleTouchIcons: [{ size: 180, href: '/favicon_io/apple-touch-icon.png' }],
  favicon: '/favicon_io/favicon.ico',
  manifest: '/favicon_io/site.webmanifest',
  children: null,
};

ReHelmet.propTypes = {
  title: PropTypes.string,
  icons: PropTypes.arrayOf(
    PropTypes.exact({
      size: PropTypes.number,
      href: PropTypes.string,
    })
  ),
  appleTouchIcons: PropTypes.arrayOf(
    PropTypes.exact({
      size: PropTypes.number,
      href: PropTypes.string,
    })
  ),
  favicon: PropTypes.string,
  manifest: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.element, PropTypes.elementType]),
};

export default ReHelmet;
