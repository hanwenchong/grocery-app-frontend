import React from 'react';
import PropTypes from 'prop-types';
// UI Framework
import { Layout, Menu, Icon } from 'antd';
// Custom UI
import Avatar from './AvatarDropdown';

const { Header } = Layout;

const HeaderUI = props => {
  const { headerProps, icon, user } = props;

  return (
    <>
      <Header {...headerProps}>
        <div className="header--global">
          <Icon {...icon} />
          <Menu mode="horizontal" className="header--right--menu">
            <Avatar user={user} />
          </Menu>
        </div>
      </Header>
    </>
  );
};

HeaderUI.propTypes = {
  headerProps: PropTypes.objectOf(PropTypes.string).isRequired,
  icon: PropTypes.shape({
    onClick: PropTypes.func,
    className: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
  user: PropTypes.shape({
    username: PropTypes.string,
  }).isRequired,
};

export default HeaderUI;
