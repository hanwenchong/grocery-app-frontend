import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// UI Framework
import { Avatar, Dropdown, Icon, Menu, Spin } from 'antd';

const AvatarDropdown = props => {
  const { user } = props;
  const menuHeaderDropdown = (
    <Menu>
      <Menu.Item>
        <Link to="/admin/logout">
          <Icon type="logout" />
          {` Log out`}
        </Link>
      </Menu.Item>
    </Menu>
  );
  const userAvatar = { icon: 'user' };

  return user && user.username ? (
    <Dropdown overlay={menuHeaderDropdown} trigger={['hover', 'click']}>
      <span className="header--menu--items">
        <Avatar size="small" {...userAvatar} alt="avatar" />
        <span>{` ${user.username}`}</span>
      </span>
    </Dropdown>
  ) : (
    <Spin size="small" style={{ marginLeft: 8, marginRight: 8 }} />
  );
};

AvatarDropdown.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    profileImage: PropTypes.string,
  }).isRequired,
};

export default AvatarDropdown;
