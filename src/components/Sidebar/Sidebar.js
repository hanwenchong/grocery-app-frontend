import React from 'react';
import { useLocation, Link } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import PropTypes from 'prop-types';
// UI Framework
import { Layout, Menu, Icon } from 'antd';
// utils
import { media } from 'utils/media';

import menuItems from './menuItems';

const { Sider } = Layout;
const { SubMenu } = Menu;

const Sidebar = props => {
  const location = useLocation();
  const paths = location.pathname.split('/').filter(path => path !== '');
  const isMobile = useMediaQuery({ maxWidth: media.sm });
  const { collapsed } = props;

  const createMenuItem = item => (
    <Menu.Item key={item.key}>
      <Link {...item.link}>
        {item.icon && <Icon {...item.icon} />}
        <span>{item.label}</span>
      </Link>
    </Menu.Item>
  );
  const createMenuTitle = item => (
    <span>
      {item.icon && <Icon {...item.icon} />}
      <span>{item.label}</span>
    </span>
  );

  return (
    <Sider {...props} collapsed={isMobile ? false : collapsed}>
      <div className="sider--logo" />
      <Menu
        mode="inline"
        defaultSelectedKeys={paths}
        defaultOpenKeys={(!collapsed && paths) || []}
        selectedKeys={paths}
        style={{ padding: '16px 0px', width: '100%', borderRight: '0' }}
        className="sider--menu"
      >
        {menuItems.map(item => {
          if (item.sub) {
            return (
              <SubMenu key={item.key} title={createMenuTitle(item)}>
                {item.sub.map(subItem => {
                  return createMenuItem(subItem);
                })}
              </SubMenu>
            );
          }
          return createMenuItem(item);
        })}
      </Menu>
    </Sider>
  );
};

Sidebar.propTypes = {
  collapsed: PropTypes.bool.isRequired,
};

export default Sidebar;
