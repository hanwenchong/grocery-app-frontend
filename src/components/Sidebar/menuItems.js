const sidebarItems = [
  {
    key: 'dashboard',
    link: {
      to: '/admin/dashboard',
    },
    icon: {
      type: 'dashboard',
    },
    label: 'Dashboard',
  },
  {
    key: 'products',
    link: {
      to: '/admin/products',
    },
    icon: {
      type: 'home',
    },
    label: 'Products',
    sub: [
      {
        key: 'product_list',
        link: {
          to: '/admin/products/product_list',
        },
        icon: {
          type: 'table',
        },
        label: 'Product List',
      },
    ],
  },
];

export default sidebarItems;
