import { createActionTypes } from 'utils/types';

const constant = createActionTypes('AUTH', [
  'SENDING_REQUEST',
  'SET_AUTH',
  'REQUEST_ERROR',
  'CLEAR_ERROR',
]);

export default constant;
