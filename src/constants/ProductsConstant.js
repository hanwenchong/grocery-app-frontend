import { createActionTypes } from 'utils/types';

const constant = createActionTypes('PRODUCTS', [
  'SENDING_REQUEST',
  'SET_PRODUCTS',
  'REQUEST_ERROR',
  'CLEAR_ERROR',
]);

export default constant;
