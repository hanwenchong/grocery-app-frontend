import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';

import routes from './routes';
import AuthRoute from './AuthRoute';
import NotAuthRoute from './NotAuthRoute';
import maps from './maps';

const switchRoutes = user => (
  <Switch>
    {routes.map(prop => {
      const routeProps = {
        key: prop.id,
        path: prop.layout + prop.path,
      };
      switch (prop.accessType) {
        case maps.AUTH:
          return <AuthRoute {...routeProps} />;
        case maps.NOT_AUTH:
          return <NotAuthRoute {...routeProps} />;
        case maps.OPTIONAL:
          return <Route {...routeProps} />;
        default:
          return null;
      }
    })}
    <Redirect from="/" exact to={user ? '/admin/dashboard' : '/auth/login'} />
    <Redirect to={user ? '/admin/404' : '/public/404'} />
  </Switch>
);

// eslint-disable-next-line
export default function RegisterPath(props) {
  const { user } = props;

  return <>{switchRoutes(user)}</>;
}

RegisterPath.defaultProps = {
  user: null,
};

RegisterPath.propTypes = {
  user: PropTypes.shape({}),
};
