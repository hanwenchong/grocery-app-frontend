import React from 'react';
import { Switch } from 'react-router-dom';
// antd components
import { Row, Col } from 'antd';
// resue components
import Helmet from 'components/Helmet';
// assets
import BrandLogo from 'assets/images/cart.png';

import routes from './routes';
import { DefaultLayoutStyle } from './style';
import NotAuthRoute from './NotAuthRoute';
import maps from './maps';

const switchRoutes = (
  <Switch>
    {routes.map(prop => {
      const routeProps = {
        key: prop.id,
        path: prop.layout + prop.path,
        component: () => (
          <>
            <Helmet title={prop.name} />
            <prop.component title={prop.name} />
          </>
        ),
      };
      switch (prop.accessType) {
        case maps.NOT_AUTH:
          return <NotAuthRoute {...routeProps} />;
        default:
          return null;
      }
    })}
  </Switch>
);

// eslint-disable-next-line
export default function DefaultLayout({ ...rest }) {
  return (
    <>
      <DefaultLayoutStyle />
      <Row
        gutter={8}
        type="flex"
        justify="space-around"
        align="middle"
        style={{ height: '100vh', width: '100%' }}
      >
        <Col xs={16} sm={16} md={24} lg={32}>
          <div className="brand">
            <img alt="brand logo" src={BrandLogo} />
          </div>
        </Col>
        <Col xs={16} sm={16} md={24} lg={32}>
          {switchRoutes}
        </Col>
        <Col xs={16} sm={16} md={24} lg={32}>
          <div className="footer">
            <div className="footer--copyright">
              {'Copyright © '}
              <a
                color="inherit"
                href="http://www.google.ca"
                target="_blank"
                rel="noreferrer noopener"
              >
                Grocery
              </a>
              {` ${new Date().getFullYear()}.`}
            </div>
          </div>
        </Col>
      </Row>
    </>
  );
}
