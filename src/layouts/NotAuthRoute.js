import React from 'react';
import { Redirect, useLocation, Route } from 'react-router-dom';
import useAuth from 'contexts/AuthContext';
import queryString from 'query-string';

const NotAuthRoute = props => {
  const location = useLocation();
  const { from } = queryString.parse(location.search);
  const {
    state: { user },
  } = useAuth();

  return user ? <Redirect to={from || '/admin/dashboard'} /> : <Route {...props} />;
};

export default NotAuthRoute;
