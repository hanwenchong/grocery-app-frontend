import React from 'react';
import shortid from 'shortid';

import maps from './maps';

// not auth pages
const LoginPage = React.lazy(() => import('views/NotAuthPage/LoginPage'));
// auth pages
const DashboardPage = React.lazy(() => import('views/AdminPage/DashboardPage'));
const LogoutPage = React.lazy(() => import('views/AdminPage/LogoutPage'));
const ProductListPage = React.lazy(() => import('views/AdminPage/ProductsPage/ProductListPage'));
// exception pages
const NotFoundPage = React.lazy(() => import('views/ExceptionPage/NotFoundPage'));

const routes = [
  // not auth routes
  {
    id: shortid.generate(),
    path: '/login',
    name: 'Login',
    layout: '/auth',
    component: LoginPage,
    accessType: maps.NOT_AUTH,
  },
  {
    id: shortid.generate(),
    path: '/404',
    name: 'Not Found',
    component: NotFoundPage,
    layout: '/public',
    accessType: maps.OPTIONAL,
  },
  // auth routes
  {
    id: shortid.generate(),
    path: '/dashboard',
    name: 'Dashboard',
    layout: '/admin',
    component: DashboardPage,
    accessType: maps.AUTH,
  },
  {
    id: shortid.generate(),
    path: '/logout',
    name: 'Logout',
    layout: '/admin',
    component: LogoutPage,
    accessType: maps.AUTH,
  },
  {
    id: shortid.generate(),
    path: '/products/product_list',
    name: 'Product List',
    component: ProductListPage,
    layout: '/admin',
    accessType: maps.AUTH,
  },
  {
    id: shortid.generate(),
    path: '/404',
    name: 'Not Found',
    component: NotFoundPage,
    layout: '/admin',
    accessType: maps.AUTH,
  },
];

export default routes;
