import React from 'react';
import { Switch } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
// UI Framework
import { Layout, Drawer } from 'antd';
// reuse components
import Sidebar from 'components/Sidebar';
import Header from 'components/Header';
import Helmet from 'components/Helmet';
// utils
import { media } from 'utils/media';

import useAuth from 'contexts/AuthContext';
import AuthRoute from './AuthRoute';
import routes from './routes';
// style
import { AuthLayoutStyle } from './style';
// maps
import maps from './maps';

const { Footer, Content } = Layout;

let firstMount = true;

const switchRoutes = (
  <Switch>
    {routes.map(prop => {
      const routeProps = {
        key: prop.id,
        path: prop.layout + prop.path,
        component: () => (
          <>
            <Helmet title={prop.name} />
            <prop.component title={prop.name} />
          </>
        ),
      };
      switch (prop.accessType) {
        case maps.AUTH:
          return <AuthRoute {...routeProps} />;
        default:
          return null;
      }
    })}
  </Switch>
);

// eslint-disable-next-line
export default function AuthLayout(props) {
  const {
    state: { user },
  } = useAuth();
  const isMobile = useMediaQuery({ maxWidth: media.sm });
  const initialSiderState = {
    collapsed: false,
    collapsedWidth: 80,
    responsive: false,
  };
  const [siderState, setSiderState] = React.useState(initialSiderState);

  const onBreakpoint = broken => {
    if (!isMobile) {
      setSiderState({
        collapsed: broken,
        collapsedWidth: 80,
      });
    }
  };

  const toggleSider = () => {
    setSiderState({
      ...siderState,
      collapsed: !siderState.collapsed,
    });
  };

  const onCollapse = collapse => {
    if (firstMount && isMobile) {
      setSiderState({
        ...siderState,
        collapsed: true,
      });
      firstMount = false;
    } else if (isMobile && collapse) {
      setSiderState({
        ...siderState,
        collapsed: true,
      });
    }
  };

  const menuSider = (
    <Sidebar
      width={256}
      breakpoint="lg"
      onBreakpoint={onBreakpoint}
      collapsedWidth={siderState.collapsedWidth}
      collapsible
      trigger={null}
      onCollapse={onCollapse}
      collapsed={siderState.collapsed}
      className="sider"
    />
  );

  return (
    <>
      <AuthLayoutStyle
        collapsed={isMobile ? true : siderState.collapsed}
        collapsedWidth={isMobile ? 0 : siderState.collapsedWidth}
      />
      <Layout>
        <Drawer
          placement="left"
          visible={!siderState.collapsed}
          onClose={() => onCollapse && onCollapse(true)}
          style={{
            padding: 0,
            height: '100vh',
            display: !isMobile ? 'none' : 'block',
          }}
        >
          {menuSider}
        </Drawer>
        {!isMobile && menuSider}

        <Layout className="layout--header-content">
          {user && (
            <Header
              headerProps={{ className: 'header' }}
              user={user}
              icon={{
                className: 'header--trigger',
                type: siderState.collapsed ? 'menu-unfold' : 'menu-fold',
                onClick: toggleSider,
              }}
            />
          )}
          <Content className="layout--content">{switchRoutes}</Content>
          <Footer className="footer">
            <div className="footer--copyright">
              {'Copyright © '}
              <a
                color="inherit"
                href="http://www.google.ca"
                target="_blank"
                rel="noreferrer noopener"
              >
                Grocery
              </a>
              {` ${new Date().getFullYear()}.`}
            </div>
          </Footer>
        </Layout>
      </Layout>
    </>
  );
}
