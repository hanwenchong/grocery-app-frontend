import React from 'react';
import { Redirect, useLocation, Route } from 'react-router-dom';
import useAuth from 'contexts/AuthContext';

const AuthRoute = ({ ...props }) => {
  const location = useLocation();
  const from = location.pathname !== '/logout' ? `?from=${location.pathname}` : '';
  const {
    state: { user },
  } = useAuth();

  return user ? <Route {...props} /> : <Redirect to={`/auth/login${from}`} />;
};

export default AuthRoute;
