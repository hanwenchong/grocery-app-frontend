const maps = {
  AUTH: 'AUTH',
  NOT_AUTH: 'NOT_AUTH',
  OPTIONAL: 'OPTIONAL',
};

export default maps;
