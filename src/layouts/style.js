import { createGlobalStyle } from 'styled-components';

export const AuthLayoutStyle = createGlobalStyle`
  .header {
    position: fixed;
    z-index: 998;
    width: calc(100% - ${props => (props.collapsed ? `${props.collapsedWidth}px` : '256px')});
    background: #fff;
    top: 0;
    right: 0;
    padding: 0;
    box-shadow: 0 1px 4px rgba(0,21,41,.08);
    transition: all 0.2s;
  }
  .layout--header-content {
    padding-left: ${props => (props.collapsed ? `${props.collapsedWidth}px` : '256px')};
    transition: all 0.2s;
    min-height: 100vh;
  }
  .footer-fixed {
    position: fixed;
    z-index: 998;
    width: calc(100% - ${props => (props.collapsed ? `${props.collapsedWidth}px` : '256px')});
    background: #fff;
    bottom: 0;
    right: 0;
    padding: 0;
    box-shadow: 0 1px 24px rgba(0,21,41,.08);
    transition: all 0.2s;
  }
  .footer--global {
    position: relative;
    height: 48px;
    padding: 0;
    background: #fff;
  }
  .footer--right--menu {
    line-height: 48px;
    float: right;
    border: none;
  }
  .sider {
    height: 100vh;
    position: fixed;
    left: 0;
    background: #fff;
    box-shadow: 0 1px 4px rgba(0,21,41,.08);
    z-index: 999;
  }
  .sider--logo {
    position: relative;
    height: 64px;
    padding-left: 24px;
    overflow: hidden;
    line-height: 64px;
    background: #fff;
    cursor: pointer;
    box-shadow: 1px 1px 0 0 #e8e8e8;
    transition: all 0.2s;
  }
  .sider--menu {
    height: calc(100vh - 64px);
    overflow-y: auto;
    overflow-x: hidden;
  }
  .header--global {
    position: relative;
    height: 64px;
    padding: 0;
    background: #fff;
    box-shadow: 0 1px 4px rgba(0,21,41,.08);
  }
  .header--right--menu {
    line-height: 64px;
    float: right;
    border: none;
  }
  .header--menu--items,
  .footer--menu--items {
    height: 100%;
    cursor: pointer;
    display: inline-block;
    padding: 0 12px;
  }
  .header--trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 24px;
    cursor: pointer;
    transition: color 0.2s;
  }
  .header--trigger:hover {
    color: #1890ff;
  }
  .layout--content {
    padding-top: 64px;
    margin: 24px;
  }
  .content {
    background: #fff;
    padding: 24px;
    margin: 24px 24px 0;
    minHeight: 280px;
  }
  .page--wrapper {
    width: 100%;
    height: 100%;
    min-height: 100%;
    min-width: 100%;
    transition: 0.2s;
    padding: 12px 24px 24px 24px;
  }
  .page--header {
    background: #fff;
  }
  .ant-drawer-body {
    padding: 0;
  }
  .footer--copyright {
    color: rgba(0,0,0,.45);
  }
  .footer {
    text-align: center;
  }
  .ant-menu-inline .ant-menu-item {
      width: 100%;
  }
  .ant-dropdown-menu-item {
      min-width: 160px;
  }
  .ant-advanced-search-form .ant-form-item {
    display: flex;
  }

  .ant-advanced-search-form .ant-form-item-control-wrapper {
    flex: 1;
  }
  .hide {
    display: none;
  }
`;

export const DefaultLayoutStyle = createGlobalStyle`
  .brand {
    text-align: center;
  }
  .brand img {
    width: 200px;
  }
  .footer--copyright {
    color: rgba(0,0,0,.45);
  }
  .footer {
    text-align: center;
  }
`;
