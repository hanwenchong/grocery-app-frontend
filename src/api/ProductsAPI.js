import qs from 'qs';

import API from './APIUtils';

export const getProducts = async (params = {}) => {
  const { pagination, sorter, search = {}, filters = {}, join = {} } = params;
  const { data } = await API.get('/products', {
    params: {
      current: (pagination && pagination.current - 1) || 0,
      pageSize: (pagination && pagination.pageSize) || 10,
      sortOrder: (sorter && sorter.order) || 'ascend',
      sortField: (sorter && sorter.columnKey) || 'name',
      search,
      filters,
      join,
    },
    paramsSerializer: query => {
      return qs.stringify(query);
    },
  });
  return data;
};

export const createProduct = async form => {
  const { data } = await API.post('/products', { ...form });
  return data;
};

export const editProduct = async form => {
  const { data } = await API.patch(`/products/${form.id}`, { ...form });
  return data;
};

export const deleteProduct = async id => {
  await API.delete(`/products/${id}`);
};
