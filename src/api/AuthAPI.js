import API, { setLocalStorage, setHeaderToken, JWT_TOKEN_KEY, clearJWT } from './APIUtils';

const handleUserResponse = ({ user }) => {
  const { token } = user;
  setLocalStorage(JWT_TOKEN_KEY, token);
  setHeaderToken(token);

  return user;
};

export const getCurrentUser = async () => {
  const { data } = await API.get(`/user`);
  return data;
};
export const login = async form => {
  const { data } = await API.post('/users/login', {
    ...form,
  });
  return handleUserResponse(data);
};
export const logout = () => {
  clearJWT();
};
