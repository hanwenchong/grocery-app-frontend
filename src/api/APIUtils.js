import axios from 'axios';
import jwtDecode from 'jwt-decode';

import { REACT_APP_API_BASE_URL, REACT_APP_API_VERSION } from 'config';

export const JWT_TOKEN_KEY = 'token';

axios.defaults.baseURL = `${REACT_APP_API_BASE_URL}/${REACT_APP_API_VERSION}`;

// eslint-disable-next-line
export function setHeaderToken(token) {
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common.Authorization;
  }
}

// eslint-disable-next-line
export function isTokenValid(token) {
  try {
    const decodedJWT = jwtDecode(token);
    const currentTime = Date.now().valueOf() / 1000;
    return decodedJWT.exp > currentTime;
  } catch (error) {
    return false;
  }
}

// eslint-disable-next-line
export function getLocalStorageValue(key) {
  const value = localStorage.getItem(key);
  if (!value) return null;
  try {
    return JSON.parse(value);
  } catch (error) {
    return null;
  }
}

// eslint-disable-next-line
export function setLocalStorage(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

export function clearJWT() {
  localStorage.removeItem(JWT_TOKEN_KEY);
  setHeaderToken(null);
}

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    switch (error.response.status) {
      case 401: // not authenticated
        clearJWT();
        window.location.pathname = '/';
        break;
      case 403: // not authorized
        // window.location.pathname = '/public/403';
        break;
      case 500: // server error
        // window.location.pathname = '/public/500';
        break;
      default:
        break;
    }
    return Promise.reject(error.response);
  }
);

export default axios;
