import { createAction } from 'utils/types';
import constant from 'constants/AuthConstant';

const action = {
  setAuth: data => createAction(constant.SET_AUTH, { ...data }),
  sendingRequest: loading => createAction(constant.SENDING_REQUEST, { ...loading }),
  requestError: error => createAction(constant.REQUEST_ERROR, { ...error }),
  clearError: () => createAction(constant.CLEAR_ERROR, { error: '' }),
};

export default action;
