import { createAction } from 'utils/types';
import constant from 'constants/ProductsConstant';

const productsAction = {
  setProducts: data => createAction(constant.SET_PRODUCTS, { ...data }),
  sendingRequest: loading => createAction(constant.SENDING_REQUEST, { ...loading }),
  requestError: error => createAction(constant.REQUEST_ERROR, { ...error }),
  clearError: () => createAction(constant.CLEAR_ERROR, { error: '' }),
};

export default productsAction;
